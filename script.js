function checkPassword(element) {
  element.preventDefault();
  let password = document.querySelector(".password");
  let passwordConfirm = document.querySelector(".confirm-password");
  if (password.value !== passwordConfirm.value || password.value === "") {
    passwordConfirm.classList.add("input_error");
    error.classList.add("show");
  } else if (password.value === passwordConfirm.value) {
    passwordConfirm.classList.remove("input_error");
    error.classList.remove("show");
    setTimeout(function () {
      alert("You are welcome");
    });
  }
}
let btn = document.querySelector(".btn");
btn.addEventListener("click", checkPassword);

let passwordForm = document.querySelector(".password-form");
passwordForm.addEventListener("click", (event) => {
  if (event.target.classList.contains("fa-eye")) {
    event.target.classList.replace("fa-eye", "fa-eye-slash");
    event.target.previousElementSibling.setAttribute("type", "text");
  } else {
    event.target.classList.replace("fa-eye-slash", "fa-eye");
    event.target.previousElementSibling.setAttribute("type", "password");
  }
});
